# Caretaker-home

----
### This project use:
* Python3.6


----
### Installation:
1. Install python packages:
    - `python -m pip install -r ./src/requirements`

2. Edit `home-settings.json` with yours settings
    
    ```json
    {
        "title": "my-caretaker-smarthome-settings",
        "version": "1.0.0",
        "home_tag": "<string>",
        "path_to_credentials": "here/path/to/file",
        "ws_to_use": "<local || server>",
        "ws": {
            "local": "<path/to/websockets/>",
            "server": "ws://api.caretaker-smarthome.eu/wss/"
        }
    }
    ```