# IMPORT PYTHON LIBRARIES ZONE
import asyncio
# END IMPORT PYTHON LIBRARIES ZONE

# IMPORT CARETAKER-HOME PACKAGES ZONE
from core import HomeHandler
# END IMPORT CARETAKER-HOME PACKAGES ZONE


class MyHome(HomeHandler):

    def __init__(self):
        HomeHandler.__init__(self)


if __name__ == "__main__":
    home = MyHome()

    loop = asyncio.get_event_loop()
    loop.run_until_complete(home.init())
    loop.run_forever()
