import json
import html
import urllib


class Helper():

    @staticmethod
    def strToByte(string):
        ''.join(format(ord(i), 'b').zfill(8) for i in string)

    @staticmethod
    def escapeHtml(string):
        return html.escape(string)

    @staticmethod
    def toJson(elmt):
        return json.dumps(elmt)

    @staticmethod
    def indexExistInArray(index, array):
        return index < len(array)

    @staticmethod
    def encodeURIComponent(string):
        return urllib.parse.quote(string, safe='~()*!.\'')
