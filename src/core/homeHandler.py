from abc import abstractmethod
import asyncio

# IMPORT CARETAKER-HOME PACKAGES ZONE
from .componentsApi import ComponentsApi
from .homeWS import HomeWS
from .homeSettings import HomeSettings
# END IMPORT CARETAKER-HOME PACKAGES ZONE


class HomeHandler(ComponentsApi):
    __settings = None
    __ws = None
    __state = None

    def __init__(self):
        self.__settings = HomeSettings()
        self.__ws = HomeWS()

        ComponentsApi.__init__(
            self, self.__settings.components, self.__ws, self.__stateHandler())

        self.__state = {
            'locked': False,
        }

    async def init(self):
        self.__ws.onRequest(self.handleRequest)
        await self.__ws.connect('homews/1238gvguz556zdzf516fzezs23azdaf6gza/')

    def __stateHandler(self):
        return {
            'getState': self.__getState,
            'setState': self.__setState
        }

    def __getState(self, element=None):
        if element != None:
            return self.__state[element]

        return self.__state

    def __setState(self, state):
        for key in state:
            self.__state[key] = state[key]
