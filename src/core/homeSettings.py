import json
import os


class HomeSettings:
    __settings = None

    def __init__(self):
        self.__loadSettings()

    def __getSettingsPath(self):
        directory = os.path.dirname(__file__)
        return os.path.join(directory, '../home-settings.json')

    def __loadSettings(self):
        with open(self.__getSettingsPath()) as settingsFile:
            self.__settings = json.load(settingsFile)

    @property
    def wsPath(self):
        wsPathToUse = self.__settings['ws_to_use']
        return self.__settings['ws'][wsPathToUse]

    @property
    def components(self):
        return self.__settings['components']
