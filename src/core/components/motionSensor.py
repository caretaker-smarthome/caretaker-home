# IMPORT PYTHON LIBRARIES ZONE
import asyncio
from gpiozero import MotionSensor
# END IMPORT PYTHON LIBRARIES ZONE


class MotionSensorHandler():
    __motionSensor = None
    __pin = None
    __whenMotionCallback = None

    def __init__(self, pin, whenMotionCallback):
        self.__pin = pin
        self.__whenMotionCallback = whenMotionCallback
        self.start()

    def start(self):
        self.__motionSensor = MotionSensor(self.__pin)
        self.__motionSensor.when_motion = self.__whenMotion
        self.__motionSensor.when_no_motion = self.__whenNoMotion

    def __whenMotion(self):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        future = asyncio.ensure_future(
            self.__executeWhenMotionCallback())
        loop.run_until_complete(future)
        loop.close()

    async def __executeWhenMotionCallback(self):
        await self.__whenMotionCallback()

    def __whenNoMotion(self):
        pass
