# IMPORT PYTHON LIBRARIES ZONE
import asyncio
from gpiozero import PWMLED
from random import random
# END IMPORT PYTHON LIBRARIES ZONE


class Fireplace():
    __lightTask = None
    __yellow1 = None
    __yellow2 = None
    __red = None

    # to immitate a fire we need 3 LEDs -> 2yellows, 1red
    # pins have to be to this format -> [yellow1pin, yellow2pin, redpin] like [7, 14, 7]
    def __init__(self, pins):
        self.__yellow1 = PWMLED(pins[0])
        self.__yellow2 = PWMLED(pins[1])
        self.__red = PWMLED(pins[2])

    async def __lightTaskCoro(self):
        while True:
            # we are using the random function to make it more realistic
            self.__yellow1.value = random()
            self.__yellow2.value = random()
            self.__red.value = random()
            await asyncio.sleep(1)

    def light(self):
        self.__lightTask = asyncio.Task(self.__lightTaskCoro())

    def extinguish(self):
        self.__yellow1.value = 0
        self.__yellow2.value = 0
        self.__red.value = 0
        self.__lightTask.cancel()
