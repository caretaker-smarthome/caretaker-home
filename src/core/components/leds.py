# IMPORT PYTHON LIBRARIES ZONE
from gpiozero import LED
# END IMPORT PYTHON LIBRARIES ZONE


class Leds():
    __leds = None

    def __init__(self, pins):
        self.__initLeds(pins)

    def __initLeds(self, pins):
        self.__leds = []

        for pin in pins:
            led = LED(pin)
            self.__leds.append(led)

    def on(self):
        for led in self.__leds:
            led.on()

    def off(self):
        for led in self.__leds:
            led.off()
