# IMPORT PYTHON LIBRARIES ZONE
import asyncio
import Adafruit_DHT
# END IMPORT PYTHON LIBRARIES ZONE


class HumidityTemperatureSensor():
    __pin = None
    __task = None
    __humidity = None
    __temperature = None

    def __init__(self, pin):
        self.__pin = pin

    async def __read(self):
        while True:
            self.__humidity, self.__temperature = Adafruit_DHT.read_retry(
                11, self.__pin)
            await asyncio.sleep(3)

    def read(self):
        self.__task = asyncio.Task(self.__read())

    def stop(self):
        self.__task.cancel()

    def getTemperature(self):
        return str(self.__temperature)

    def getHumidity(self):
        return str(self.__humidity)
