# IMPORT PYTHON LIBRARIES ZONE
import asyncio
import RPi.GPIO as GPIO
# END IMPORT PYTHON LIBRARIES ZONE


class SmokeSensor():
    """
    Attributes
    -----------
    pin : int
        Pin where the smoke sensor is connected
    onSmokeDetection: callback
        Callback that will be executed on smoke detection
    """

    __pin = None
    __task = None
    __onSmokeDetectionCallback = None

    def __init__(self, pin, onSmokeDetection):
        self.__pin = pin
        self.__onSmokeDetectionCallback = onSmokeDetection

    async def __taskCoro(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.__pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.add_event_detect(self.__pin, GPIO.RISING)
        GPIO.add_event_callback(self.__pin, self.__onSmokeDetection)

    def read(self):
        self.__task = asyncio.Task(self.__taskCoro())

    async def __onSmokeDetection(self, pin):
        await self.__onSmokeDetectionCallback()
