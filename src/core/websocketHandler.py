# IMPORT PYTHON LIBRARIES ZONE
from abc import ABC, abstractmethod
import asyncio
import json
import websockets
import sys
# END IMPORT PYTHON LIBRARIES ZONE

# IMPORT CARETAKER-HOME PACKAGES ZONE
from .homeSettings import HomeSettings
# END IMPORT CARETAKER-HOME PACKAGES ZONE


class WebsocketHandler(ABC):
    __PATH_TO_WS = None
    __connection = None
    isIdentified = False

    def __init__(self):
        self.__setPathToWs()

    def __setPathToWs(self):
        settings = HomeSettings()
        self.__PATH_TO_WS = settings.wsPath

    async def __listenForMessages(self):
        while True:
            # await statements to switch between coroutines
            await asyncio.sleep(0)

            try:
                message = await self.__connection.recv()

                await self.onMessage(json.loads(message))

            except self.__connection.ConnectionClosed as cc:
                print("Connection closed -> " + cc)

            except Exception as e:
                print("Something happened -> " + e)

    async def __connect(self, wsPath):
        fullPathToWs = self.__PATH_TO_WS + wsPath
        print("\n>>> Try to connect to : " + fullPathToWs)

        try:
            self.__connection = await websockets.connect(fullPathToWs)
            asyncio.ensure_future(self.__listenForMessages())
            print(">>> Connected")
        except Exception as e:
            print('::: Impossible to connect to ' +
                  fullPathToWs + '\n' + str(e.args))
            sys.exit()

    async def connect(self, wsPath):
        await self.__connect(wsPath)

    async def send(self, data):
        dataAsString = json.dumps(data)
        await self.__connection.send(dataAsString)
        # print("> Message Send -> " + dataAsString)

    async def close(self):
        await self.__connection.close()

    @abstractmethod
    async def onMessage(self, msg):
        pass
