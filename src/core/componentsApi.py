import asyncio

from .helper import Helper

# IMPORT CARETAKER-HOME COMPONENTS ZONE
from .components import *
# END IMPORT CARETAKER-HOME COMPONENTS ZONE


class ComponentsApi():
    """
    Attributes
    -----------
    components : list
        list of the components of your home-settings.json
    ws : HomeWS
        object to communicate via websockets to Caretaker-backend server
    homeState: dict
        dict that contain multiples functions like 'getState' or 'setState' to get home infos
    """

    __homeState = None
    __ws = None
    __components = {}
    __tasks = {}

    def __init__(self, components, ws, homeState):
        self.__homeState = homeState
        self.__ws = ws
        self.__initComponents(components)

    def __initComponents(self, components):
        for componentName in components:
            component = components[componentName]

            if ('enabled' not in component) or component['enabled']:
                self.__components[componentName] = {
                    'type': component['type'],
                    'component': self.__getComponent(component['type'], component['pin'])
                }

                print("\nInit component: " + componentName)
                print("  type: " + component['type'])

    def __getComponent(self, componentType, pin):
        if componentType == 'fireplace':
            return Fireplace(pin)
        elif componentType == 'motionSensor':
            return MotionSensorHandler(pin, self.__whenMotionDetection)
        elif componentType == 'humidityTemperaturesSensor':
            return HumidityTemperatureSensor(pin)
        elif componentType == 'smokeSensor':
            return SmokeSensor(pin, self.__onSmokeDetection)
        elif componentType == 'leds':
            return Leds(pin)

    def __isComponent(self, componentName):
        return componentName in self.__components

    async def __handleInvalidRequest(self, invalidElementType, componentName, action, value):
        """
        Parameters
        ----------
        invalidElementType : str
            The invalid element type in the request (component || action || value)
        componentName : str
            The request component
        action : str
            The request action
        value : str
            The request value
        """
        request = f'{componentName}/{action}/{value}'
        if invalidElementType == 'component':
            errorMessage = f'Invalid component name [{componentName}] in request {request}'
        elif invalidElementType == 'action':
            errorMessage = f'Invalid action [{action}] in request {request}'
        elif invalidElementType == 'value':
            errorMessage = f'Invalid value [{value}] in request {request}'

        await self.__ws.sendRequest(f'client/error/"{errorMessage}"')
        print('::: ' + errorMessage)

    async def handleRequest(self, request):
        print(request)
        splittedRequest = request.split('/')
        subject = splittedRequest[0]
        action = splittedRequest[1]
        actionValue = None

        if Helper.indexExistInArray(2, splittedRequest):
            actionValue = splittedRequest[2]

        if self.__isComponent(subject):
            component = self.__components[subject]

            if component['type'] == 'fireplace':
                await self.__handleFireplaceRequest(
                    subject, action, actionValue)

            elif component['type'] == 'humidityTemperaturesSensor':
                await self.__handleHumidityTemperaturesSensorRequest(
                    subject, action, actionValue)
            elif component['type'] == 'leds':
                await self.__handleLedsRequest(subject, action, actionValue)
        elif subject == 'component':
            await self.__handleComponentRequest(action, actionValue)
        elif subject == 'identification':
            await self.__handleIdentificationtRequest(action, actionValue)
        else:
            await self.__handleInvalidRequest(
                'component', subject, action, actionValue)

    async def __handleComponentRequest(self, action, value):
        if action == 'get':
            UriEncodedcomponentsJson = Helper.encodeURIComponent(
                Helper.toJson(self.__getComponentsWithoutComponentAttribute()))
            await self.__ws.sendRequest('client/component/' + UriEncodedcomponentsJson)

    async def __handleIdentificationtRequest(self, action, value):
        if action == 'success':
            print('>>> Home connexion has succeed')
        elif action == 'requested':
            homeKey = '123test'
            await self.__ws.sendRequest('server/identification/' + homeKey)

    async def __handleFireplaceRequest(self, componentName, action, value):
        if action == 'turn':
            if value == 'on':
                self.__components[componentName]['component'].light()
                await self.__ws.sendRequest('client/' + componentName + '/turn/on')
            elif value == 'off':
                self.__components[componentName]['component'].extinguish()
                await self.__ws.sendRequest('client/' + componentName + '/turn/off')
            else:
                await self.__handleInvalidRequest(
                    'value', componentName, action, value)
        else:
            await self.__handleInvalidRequest(
                'action', componentName, action, value)

    async def __handleLedsRequest(self, componentName, action, value):
        if action == 'turn':
            if value == 'on':
                self.__components[componentName]['component'].on()
                await self.__ws.sendRequest('client/' + componentName + '/turn/on')
            elif value == 'off':
                self.__components[componentName]['component'].off()
                await self.__ws.sendRequest('client/' + componentName + '/turn/off')
            else:
                await self.__handleInvalidRequest(
                    'value', componentName, action, value)
        else:
            await self.__handleInvalidRequest(
                'action', componentName, action, value)

    async def __handleHumidityTemperaturesSensorRequest(self, componentName, action, value):
        if action == 'read':
            if value == 'start':
                self.__components[componentName]['component'].read()
                self.__tasks[componentName] = asyncio.Task(
                    self.__sendHumidityTemperatures(componentName))
            elif value == 'stop':
                self.__components[componentName]['component'].stop()
                self.__tasks[componentName].cancel()
            else:
                await self.__handleInvalidRequest(
                    'value', componentName, action, value)
        else:
            await self.__handleInvalidRequest(
                'action', componentName, action, value)

    async def __sendHumidityTemperatures(self, componentName):
        while True:
            temperature = self.__components[componentName]['component'].getTemperature(
            )
            humidity = self.__components[componentName]['component'].getHumidity(
            )

            await self.__ws.sendRequest(f'client/{componentName}/temperature/{temperature}')
            await self.__ws.sendRequest(f'client/{componentName}/humidity/{humidity}')

            await asyncio.sleep(1)

    async def __onSmokeDetection(self):
        await self.__ws.sendRequest('client/smokeDetection/true')

    async def __whenMotionDetection(self):
        await self.__ws.sendRequest('client/motionDetection/true')
        # TODO: Fix bug
        '''
        if self.__homeState['getState']['locked']:
            await self.__ws.sendRequest('client/motionDetection/true')
        '''

    def __getComponentsWithoutComponentAttribute(self):
        components = {}

        for componentName in self.__components:
            components[componentName] = {
                'type': self.__components[componentName]['type']
            }

        return components
