# IMPORT PYTHON LIBRARIES ZONE
import asyncio
import json
# END IMPORT PYTHON LIBRARIES ZONE

# IMPORT CARETAKER-HOME PACKAGES ZONE
from core import WebsocketHandler
# END IMPORT CARETAKER-HOME PACKAGES ZONE


class HomeWS(WebsocketHandler):

    __onRequestCallback = None

    def __init__(self):
        super().__init__()

    async def onMessage(self, msg):
        # print("> New message -> " + json.dumps(msg))

        try:
            request = msg['body']['request']
            await self.__onRequestCallback(request)
        except IndexError:
            print("No request given in this message")

    def __getUniformMessage(self):
        data = {
            'header': {
                'sender_type': 'home',
                'private_key': '123Soleil',
            },
            'body': {}
        }

        return data

    def onRequest(self, callback):
        self.__onRequestCallback = callback

    async def sendRequest(self, request):
        """
        Parameters
        ----------
        request : str
            String that will be send to server
        """
        uniformedMessage = self.__getUniformMessage()
        uniformedMessage['body']['request'] = request

        await self.send(uniformedMessage)
